#! /bin/sh
for i in "$@" ; do
  echo "$i : "

 if [ -L $i ]; then echo "Lien symbolique"; fi
 if [ ! -e $i ]; then 
     echo " n'existe pas "
     continue
 fi

 echo -n " type = "
    [ -f $i ]&& echo "fichier ordinaire"
    [ -b $i ]&& echo "fichier bloc" 
    [ -p $i ]&& echo "tube nommé"
    [ -S $i ]&& echo "fichier socket"
    [ -d $i ]&& echo "repertoire"
    [ -c $i ]&& echo "caractere"

 echo -n "acces = "
    [ -r $i ]&& echo -n "lecture "
    [ -w $i ]&& echo -n "Ecriture "
    [ -x $i ]&& echo -n "exécuter "

 echo  ""

    [ -G $i ]&& echo "notre GID"
    [ -O $i ]&& echo "notre UID"

done
