#! /bin/bash
# cleanup
# a executer en tant que root 

REP_TRACES=/var/log
# Les variables sont préférées aux valeurs codées en dur

#cd /var/log
#cd $REP_TRACES
UID_ROOT=0   # Seuls les utilisateurs avec un $UID valant 0 ont les droits de root
LIGNES=50    # Nombre de ligne sauvegardée par défaut
E_XCD=66     # On ne peut pas changer de repertoire
E_NONROOT=67 # Code de sortie si non root 

# A execution en tant que root
if [ "$UID" -ne "$UID_ROOT" ]
then
  echo "Vous devez être root pour exécuter ce script."
  exit $E_NONROOT
fi

if [ -n "$1" ]
# Teste si un argument est présent en ligne de commande (non vide)
then
   lignes=$1
else
  lignes=$LIGNES # Par défaut, s'il n'est pas spécifié sur la ligne de commande
fi

#   E_MAUVAISARGS=65    

cd $REP_TRACES
if [ `pwd` != "$REP_TRACES" ]  # ou if [ "$PWD" != "$REP_TRACES" ]
                               # Pas dans /var/log ?
then
   echo "Impossible d'aller dans $REP_TRACES"
   exit $E_XCD
fi # Double vérification du bon répertoire

tail -n $lignes messages > mesg.temp # Sauvegarde la derniere section du journal de traces
mv mesg.temp messages                # Devient le nouveau journal de traces


#cat /dev/null > messages
cat /dev/null > wtmp

echo "Journaux nettoyés"

exit 0 # la bonne méthode pour "sortir" d'un script
