#! /bin/sh

REPERTOIRE=~/scans
PREFIXE=scan

# Chercher le premier numero libre de la journée 
base_fichier="${REPERTOIRE}/${PREFIXE}"-$(date +"%Y-%m-%d")
i=1
while true
do
    fichier="${base_fichier}"-$(printf "%02d" $i).pdf
    if [ -e "$fichier" ]
    then 
       i=$((i+1))
       continue
    fi
    break
done

while true
do
    echo "Placez le document sur le scanner"
    echo " pressez Entrée (Ctrl-D pour finir)"
    read r || break
    scanimage > scan.pnm || break
    convert scan.pnm "$fichier" || break
    echo "===> fichier $fichier sauvegarde"
    i=$((i+1))
    fichier="${base_fichier}"-$(printf "%02d" $i).pdf
done
