#! /bin/sh

FICHIERSAUVE=backup-$(date +%m-%d-%Y)

archive=${1:-$FICHIERSAUVE}

tar cvf - `find . -mtime -1 -type f -print` > $archive.tar
gzip $archive.tar
echo "Repertoire $PWD sauvegarde dans un fichier archive \"$archive.tar.gz\"."
exit 0

# find . -mtime -l -type f -print0 | xargs -0 tar rvf "$archive.tar"
#  find . -mtime -1 -type f -exec tar rvf "$archive.tar" '{}' \;
