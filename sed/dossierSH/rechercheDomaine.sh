#! /bin/sh

E_SANSARGS=65
 
if [ -z "$1" ]
then
    echo "Usage: `basename $0` [nom-domaine]"
    exit $E_SANSARGS
fi

 #Vérifier le nom du script et appelle le bon serveur

case `basename $0` in 
    "wh"    ) whois $1@whois.ripe.net;;
    "wh-ripe"   ) whois $1@whois.ripe.net;;
    "wh-radb"    ) whois $1@whois.radb.net;;
    "wh-cw"    ) whois $1@whois.cw.net;;
    *    ) echo "Usage: `basename $0` [nom-domaine]";;
esac

exit $?

