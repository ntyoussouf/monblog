#! /bin/sh

# La 1ere colonne correspond aux noms ou adresses IP
# des postes a surveiller, la seconde aux nom d'utilisateur
# a employer pour la supervision
i=1
poste[$i]="192.168.133.141" ; user[$i]="root"; i=$((i+1))
poste[$i]="192.168.133.142" ; user[$i]="root"; i=$((i+1))
poste[$i]="192.168.133.143" ; user[$i]="root"; i=$((i+1))
poste[$i]="192.168.133.144" ; user[$i]="root"; i=$((i+1))
poste[$i]="192.168.133.145" ; user[$i]="root"; i=$((i+1))
nb_postes=$i

installer()
{
  # Generer si besoin la cle ssh 
  if [ ! -f ~/.ssh/id_rsa.pub ]
  then 
      rm -rf ~/.ssh
      ssh-keygen -t rsa
  fi

  # Aller l'inscrire sur les postes supervises
  cle=$(cat ~/.ssh/id_rsa.pub)
  local i=1
  while [ $i -lt $nb_postes ]; do
  ssh -o StrictHostKeyChecking=no ${user[$i]}@${poste[$i]} \
  "rm -rf .ssh; mkdir .ssh; echo $cle > .ssh/authorized_keys"
       i=$((i+1))
  done
}

executer()
{
  local i=1
  while [ $i -lt $nb_postes ]
  do
      echo "=====${poste[$i]}===="
      ssh ${user[$i]}@${poste[$1]} "$*"
      i=$((i+1))
  done
}


download()
{
  local r="$1"
  local p="$2";
  local i=1
  while [ $1 -lt $nb_postes ]
  do
    if ping -c 1 -w ${poste[$i]} >/dev/null 2>$1
    then 
       echo "${poste[$i]}"
       scp  "${user[$i]}@${poste[$1]}:$r" "$p"-${poste[$i]}
    fi
    i=$((i+1))
  done
}

upload()
{
  local l="$1" 
  local r="$2"
  local i=1
  while [ $i -lt $nb_postes ]
  do 
    if ping -c 1 -w 1 ${poste[$1]} >/dev/null 2>&1
    then  
       echo "${poste[$i]}"
       scp "$1"  "${user[$i]}@${poste[$1]}:$r" 
    fi
    i=$((i+1))
  done
}

if [ "$*" = "" ]; then cat <<- EOF
  USAGE:
   $0 action [arguments...]
  ACTIONS
   install
     installer la supervision sur tous les postes 
   upload local remote
   copier le fichier "local" a l'emplacement "distant"
   sur toutes les stations
   download distant prefixe
   copier tous les fichiers distants en local avec le prefixe
   suivi du nom de station
   execute commande...
   executer la "commande" sur un shell distant
  EOF
  exit 0
fi

case $1 in 
  instal* )
   shift
   install ;;
download )
   shift
   if [ $# -ne 2 ]; then echo "err. d arguments"; exit 2; fi
   download "$1" "$2"
   ;; 
upload )
   shift
   if [ $# -ne 2 ]; then echo "err. d arguments"; exit 2; fi
   upload "$1" "$2"
   ;; 
exec* )
   shift
   executer "$@" ;;  
* )
   echo "Action inconnue." >&2
   echo "Invoquez \"$0\" seul pour avoir de l'aide" >&2
   exit 1
   ;;
esac
