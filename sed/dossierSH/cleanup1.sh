#! /bin/bash
# cleanup
# a executer en tant que root 

REP_TRACES=/var/log
# Les variables sont préférées aux valeurs codées en dur

#cd /var/log
cd $REP_TRACES

cat /dev/null > messages
cat /dev/null > wtmp

echo "Journaux nettoyés"

exit # la bonne méthode pour "sortir" d'un script
