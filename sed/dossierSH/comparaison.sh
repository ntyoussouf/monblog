#! /bin/sh

leg1=carottes
leg2=tomates

if [[ "$leg1" < "$leg2" ]]
then
   echo -n "Le fait que $leg1 précède $leg2 dans le dictionnaire "
   echo " n' a rien à voir avec mes préférences culinaires."
else
   echo "Mais quel type de dictionnaire utilisez-vous?"
fi

