#!/bin/sh

HELP_=false
MAN_=false

while getopts hm o
do case $0 in
   h) HELP_=true;;
   m) MAN_=true;;
  [?]) print >&2 "Usage: $0 [-s] [ -d seplist ] file ..."
               exit 1;;
   esac
done

echo $HELP_
echo $MAN_

shift $OPTIND-1
echo $#
