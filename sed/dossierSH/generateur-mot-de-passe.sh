#! /bin/bash
lg_mot_de_passe="${1:-8}"
mot_de_passe=""

# Liste des caractéristiques utilisables pour le mot de passe
caracteres="ABCDEFGHIJKLMNOPQRSTUVWXZ"
caracteres="${caracteres}abcdefghijklmnopqrstuvwxz"
caracteres="${caracteres}0123456789"
# NB: le caractère $ doit etre protege par un backslash.
caracteres="${caracteres}\$@&%~#{([-_])}+="

# Nombre de caractères dans la liste de ceux autorises
nb_caracteres=${#caracteres}

i=1
while [ $i -le "$lg_mot_de_passe" ]
do
  # Tirer une valeur aleatoire entre 1 et le nombre de caractères dans la liste autorisee.
  n=$((1 + ${RANDOM} % ${nb_caracteres}))
  # Ajouter dans le mot de passe le n-ieme caractere
  i=$((i + 1))
done
echo "$mot_de_passe"
