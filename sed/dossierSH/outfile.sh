#! /bin/sh
while true
do
if [ -f $outfile ]
then
    echo "Output file [$outfile] already exists"
    echo "Okay to overwrite? (y/n) : \c"
    read answer
    if [ "$answer" = "n" ] || [ "$answer" = "N" ]
    then
    echo "Aborting"
    exit
    fi
   
fi
done

