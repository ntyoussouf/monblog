#! /bin/sh

if [ $# -ne 1 ]
then
   echo "usage: $0 fichier.sql" >&2
   exit 1
fi

# (Re)creer la table
sqlite "$1" <<- EOF 
   CREATE TABLE IF NOT EXITS depts ( 
       numero    INT,
       nom      STRING,
       prefecture STRING,
       distance  INT 
);
EOF

ajouter_dept()
{
  sqlite3 "$1" <<- EOF
    DELETE FROM depts WHERE numero="$2";
    INSERT INTO depts VALUES("$2", "$3", "$4", "$5");
  EOF
}

ajouter_dept "$1" "01" "Ain"     "Bourg-en-Bresse"   429
ajouter_dept "$1" "02" "Aisne"     "Laon"            138
ajouter_dept "$1" "03" "Allier"     "Moulin"         300
ajouter_dept "$1" "971" "Guadeloupe"     "Basse-terre"   -1
ajouter_dept "$1" "972" "Martique"     "fort-de-france"   -1
ajouter_dept "$1" "973" "Guyane"     "Guyanne"   -1
ajouter_dept "$1" "974" "La réunion"     "Saint-Denis"   429
