#! /bin/sh

reponse()
{
 local rep
 while true
 do
    echo -n "$@ (O/N) " >&2
    read rep
    case "$rep" in
          [OoYy]* ) return 0 ;;
	  [Nn]*)    return 1 ;;
    esac
 done
}

#repose
mini=1
maxi=1000

echo "choissez un nombre"
while true
do 
   milieu=$(( (mini+maxi) / 2 ))
   reponse "Le nombre est supérieur ou égal a $milieu"
   if [ $? -eq 0 ]
   then
       mini=$milieu
   else
       maxi=$milieu
   fi
   if [ $mini -eq $maxi] || [ $mini -eq $((maxi - 1)) ]
   then
       echo "Le nombre est $mini"
       break
   fi
done




























