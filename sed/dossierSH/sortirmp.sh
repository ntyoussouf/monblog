#! /bin/sh

SUCCES=0
E_SANSARGS=65

if [ -z "$1" ]
then
        echo "Usage: `basename $0` fichier-rpm"
      exit $E_SANSARGS
fi
{ # Début du bloc de code 
  echo 
  echo "Description de l'archive :"
  rpm -qpi $1    # Requête pour la description
  echo
  echo "Contenu de learchive "
  rpm -qpl $1   # Requête pour la liste
  echo 
  rpm -i --test $1 # Requête pour savoir si le fichier rpm est installable
  if [ "$?" -eq $SUCCES ]
  then
    echo "$1 est installable."
  else
    echo "$1 n'est pas installable."
  fi
  echo # Fin du bloc de code
} > "$1.test"       # Redirige la sortie de tout le bloc vers un fichier

echo "Les résultats du test rpm sont dans le fichier $1.test"
# Voir la page 
exit 0
