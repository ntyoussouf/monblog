#! /bin/sh

max=5
min=0

while [ $min -ne $max ]
do
   echo "$1"
   sleep 1
   min=$(expr $min + 1)
done

