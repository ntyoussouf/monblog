#! /bin/sh

for i in "$@" ; do
    echo "$i : "
    if [ -L "$i" ] ; then echo " (lien symbolique )" ; fi
    if [ ! -e "$i" ] ; then 
       echo " n'existe pas"
       continue
    fi
    echo -n "  type= "
        [ -b "$i" ] && echo "spécial bloc"
        [ -c "$i" ] && echo "spécial caractère"
        [ -d "$i" ] && echo "repertoire"
        [ -f "$i" ] && echo "fichier regulier"
        [ -p "$i" ] && echo "tube nommé"
        [ -S "$i" ] && echo "socket"
    echo -n " acces = "
        [ -r "$i" ] && echo -n "lecture"
        [ -w "$i" ] && echo -n "écriture"
        [ -x "$i" ] && echo -n "execution"
    echo ""
        [ -G "$i" ] && echo "a notre GID"
        [ -O "$i" ] && echo "a notre UID"
done

