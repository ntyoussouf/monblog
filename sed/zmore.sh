#! /bin/sh

SANSARGS=65
PASTROUVE=66
MONGZIP=67

if [ $# -eq 0 ]
then
   echo "Usage: `basename $0` nomfichier" >&2
   exit $SANSARGS
fi

echo

monfichier=$1
if [ ! -f "$nomfichier" ]
then
   echo "Fichier $nomfichier introuvable !"  >&2
   exit $PASTROUVE
fi
echo
if [ ${nomfichier##*.} != "gz" ]

then
   echo "Le fichier $1 n'est pas compressé avec gzip"
   exit $NONGZIP
fi

zcat $1 | more

exit $?
