#! /bin/sh

for i in $@
do
   echo "saisir \c "
   echo " $i :"

   if [ -L $i ] ; then echo " Lien symbolique"; fi
   if [ ! -e $i ] ;  then
    echo "saisie vide"
    continue
   fi
  
   echo -n "type = " 
    [ -f $i ] && echo "fichier regulier"
    [ -d $i ] && echo "repertoire"
    [ -S $i ] && echo "fichier socket"
    [ -b $i ] && echo "spicial block"
    [ -p $i ] && echo "pipe péripherique"
  
   echo -n  "acess = "
    [ -r "$i" ] && echo -n "read "
    [ -w "$i" ] && echo -n "write "
    [ -x "$i" ] && echo -n "execute "

   echo   ""
    [ -G $i ] && echo " a notre GID"
    [ -O $i ] && echo "a notre UID"
done
